﻿using System;
using System.Text;

namespace GoogleLeetCode.AddStrings
{
    public class Solution
    {
        private EqualLengthStrings equalLengthStrings;

        public Solution()
        {
            equalLengthStrings = new EqualLengthStrings();
        }

        public string AddStrings(string num1, string num2)
        {
            equalLengthStrings.MakeLengthEqual(num1, num2);

            num1 = equalLengthStrings.Num1;
            num2 = equalLengthStrings.Num2;
            var addOneOnNextStep = false;
            var res = new StringBuilder();

            for (var i = num1.Length - 1; i >= 0; i--)
            {
                var extraNumber = addOneOnNextStep ? 1 : 0;
                var sum = char.GetNumericValue(num1[i]) + char.GetNumericValue(num2[i]) + extraNumber;
                addOneOnNextStep = sum > 9;

                res.Append(addOneOnNextStep ? (sum - 10) : sum);
            }

            if (addOneOnNextStep)
            {
                res.Append("1");
            }

            return new ReverseString(res.ToString()).Value;
        }
    }

    public class ReverseString
    {
        private string val;

        public string Value
        {
            get
            {
                var chars = val.ToCharArray();
                Array.Reverse(chars);

                return new string(chars);
            }
        }

        public ReverseString(string s)
        {
            val = s;
        }
    }

    public class EqualLengthStrings
    {
        private string num1;
        private string num2;

        public string Num1 => num1;
        public string Num2 => num2;

        public EqualLengthStrings()
        {
        }

        public void MakeLengthEqual(string num1, string num2)
        {
            if (num1.Length == num2.Length)
            {
                this.num1 = num1;
                this.num2 = num2;
            }
            else if (num1.Length < num2.Length)
            {
                var extraNumber = num2.Length - num1.Length;
                var zerosPrefix = new string('0', extraNumber);
                this.num1 = zerosPrefix + num1;
                this.num2 = num2;
            }
            else
            {
                var extraNumber = num1.Length - num2.Length;
                var zerosPrefix = new string('0', extraNumber);
                this.num1 = num1;
                this.num2 = zerosPrefix + num2;
            }
        }
    }
}