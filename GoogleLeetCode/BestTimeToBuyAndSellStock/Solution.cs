﻿namespace GoogleLeetCode.BestTimeToBuyAndSellStock
{
    public class Solution
    {
        public int MaxProfit(int[] prices)
        {
            if (prices == null)
            {
                return 0;
            }

            if (prices.Length == 0)
            {
                return 0;
            }

            var profit = 0;
            var minPrice = int.MaxValue;

            for (var i = 0; i < prices.Length; i++)
            {
                var currentDayPrice = prices[i];

                if (currentDayPrice < minPrice)
                {
                    minPrice = currentDayPrice;
                    continue;
                }

                var possibleProfitOnCurrentDay = currentDayPrice - minPrice;

                if (possibleProfitOnCurrentDay > profit)
                {
                    profit = possibleProfitOnCurrentDay;
                }
            }

            return profit;
        }
    }
}