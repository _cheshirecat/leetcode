﻿using System.Collections.Generic;

namespace GoogleLeetCode.HappyNumber
{
    public class Solution
    {
        public bool IsHappy(int n)
        {
            var inputAsString = n.ToString();
            var hash = new HashSet<int>();

            while (true)
            {
                var res = 0;

                for (var i = 0; i < inputAsString.Length; i++)
                {
                    var charToInt = int.Parse(inputAsString[i].ToString());
                    res += charToInt * charToInt;
                }

                if (res == 1)
                {
                    return true;
                }
                else if (hash.Contains(res))
                {
                    break;
                }
                else
                {
                    inputAsString = res.ToString();
                    hash.Add(res);
                }
            }

            return false;
        }
    }
}