﻿using System;
using System.Diagnostics;

namespace GoogleLeetCode.LongestPalindromicSubstring
{
    public class Solution
    {
        public string LongestPalindrome(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            var inputLength = input.Length;

            if (inputLength == 1)
            {
                return input;
            }

            if (inputLength == 2)
            {
                return input[0] == input[1] ? input : input[0].ToString();
            }

            bool[,] matrix = new bool[inputLength, inputLength];
            var resultStartIndex = 0;
            var resultEndIndex = 0;

            // len = 1
            for (var i = 0; i < inputLength; i++)
            {
                matrix[i, i] = true;
            }

            // len = 2
            for (var i = 1; i < inputLength; i++)
            {
                var startIndex = i - 1;
                var endIndex = i;

                matrix[startIndex, endIndex] = input[startIndex] == input[endIndex];

                if (matrix[startIndex, endIndex])
                {
                    resultStartIndex = startIndex;
                    resultEndIndex = endIndex;
                }
            }

            for (var currentStartIndex = 2; currentStartIndex <= inputLength - 1; currentStartIndex++)
            {
                var countOfSubstrings = inputLength - currentStartIndex;

                for (var substringStartIndex = 0; substringStartIndex < countOfSubstrings; substringStartIndex++)
                {
                    var currentEndIndex = substringStartIndex + currentStartIndex;

                    matrix[substringStartIndex, currentEndIndex] =
                        matrix[substringStartIndex + 1, currentEndIndex - 1]
                        && input[substringStartIndex] == input[currentEndIndex];

                    if (matrix[substringStartIndex, currentEndIndex])
                    {
                        resultStartIndex = substringStartIndex;
                        resultEndIndex = currentEndIndex;
                    }
                }
            }

            var resLength = resultEndIndex - resultStartIndex + 1;
            var res = input.Substring(resultStartIndex, resLength);

            return res;
        }
    }
}