﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleLeetCode.MinStack
{
    public class MinStack
    {
        private Stack<int> stack;
        private Stack<int> minStack;

        public MinStack()
        {
            stack = new Stack<int>();
            minStack = new Stack<int>();
        }

        public void Push(int x)
        {
            stack.Push(x);

            if (minStack.Count == 0)
            {
                minStack.Push(x);
            }
            else
            {
                var currentMin = GetMin();
                if (x <= currentMin)
                {
                    minStack.Push(x);
                }
            }
        }

        public void Pop()
        {
            if (stack.Pop() == GetMin())
            {
                minStack.Pop();
            }
        }

        public int Top()
        {
            return stack.Peek();
        }

        public int GetMin()
        {
            return minStack.Peek();
        }
    }
}