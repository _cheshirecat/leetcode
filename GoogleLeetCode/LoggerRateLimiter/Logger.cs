﻿using System.Collections.Generic;

namespace GoogleLeetCode.LoggerRateLimiter
{
    public class Logger
    {
        private Dictionary<string, int> log;

        public Logger()
        {
            log = new Dictionary<string, int>();
        }

        public bool ShouldPrintMessage(int timestamp, string message)
        {
            if (log.ContainsKey(message))
            {
                var lastTimestampOfMessage = log[message];
                var messageWasLoggedInLastTenSecond = (lastTimestampOfMessage + 10) > timestamp;

                if (messageWasLoggedInLastTenSecond)
                {
                    return false;
                }
                else
                {
                    log[message] = timestamp;

                    return true;
                }
            }
            else
            {
                log[message] = timestamp;

                return true;
            }
        }
    }
}