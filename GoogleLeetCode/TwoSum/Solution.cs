﻿namespace GoogleLeetCode.TwoSum
{
    public class Solution
    {
        public int[] TwoSum(int[] nums, int target)
        {
            if (nums == null)
            {
                return new int[] { };
            }

            if (nums.Length < 2)
            {
                return new int[] { };
            }

            for (var i = 0; i < nums.Length - 1; i++)
            {
                for (var j = i + 1; j < nums.Length; j++)
                {
                    var sum = nums[i] + nums[j];
                    if (sum == target)
                    {
                        return new int[] { i, j };
                    }
                }
            }

            return new int[] { };
        }
    }
}