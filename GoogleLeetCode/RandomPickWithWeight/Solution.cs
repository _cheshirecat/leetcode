﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleLeetCode.RandomPickWithWeight
{
    public class Solution
    {
        private int[] sums;
        private int totalSum;

        public Solution(int[] w)
        {
            sums = new int[w.Length];

            for (int i = 0; i < w.Length; ++i)
            {
                totalSum += w[i];
                sums[i] = totalSum;
            }
        }

        public int PickIndex()
        {
            var target = totalSum * new Random().NextDouble();
            int i = 0;

            for (; i < sums.Length; ++i)
            {
                if (target < sums[i])
                {
                    return i;
                }

            }

            return i - 1;
        }
    }
}
