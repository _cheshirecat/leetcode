﻿using System;
using System.Linq;

namespace GoogleLeetCode.MergeSortedArray
{
    public class Solution
    {
        public void Merge(int[] nums1, int m, int[] nums2, int n)
        {
            var list = nums1.ToList();
            list.AddRange(nums2);
            nums2 = list.ToArray();
            Array.Sort(nums2);
        }
    }
}