﻿namespace GoogleLeetCode.RepeatedSubstringPattern
{
    public class Solution
    {
        public bool RepeatedSubstringPattern(string s)
        {
            var maxLengthOfSubstring = s.Length / 2;

            for (var i = 0; i < maxLengthOfSubstring; i++)
            {
                var lengthOfSubstring = (i + 1);
                var indexWorks = (s.Length % lengthOfSubstring) == 0;

                if (!indexWorks)
                {
                    continue;
                }

                var timesToRepeat = s.Length / lengthOfSubstring;
                var substringToCheck = s.Substring(0, lengthOfSubstring);
                var temp = substringToCheck;

                for (var j = 1; j < timesToRepeat; j++)
                {
                    temp += substringToCheck;

                    if (!s.StartsWith(temp))
                    {
                        break;
                    }
                }

                if (s == temp)
                {
                    return true;
                }
            }

            return false;
        }
    }
}