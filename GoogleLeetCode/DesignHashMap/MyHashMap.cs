﻿using System.Collections.Generic;

namespace GoogleLeetCode.DesignHashMap
{
    public class MyHashMap
    {
        private Dictionary<int, int> innerDict;

        public MyHashMap()
        {
            innerDict = new Dictionary<int, int>();
        }

        public void Put(int key, int value)
        {
            innerDict[key] = value;
        }

        public int Get(int key)
        {
            return innerDict.ContainsKey(key) ? innerDict[key] : -1;
        }

        public void Remove(int key)
        {
            innerDict.Remove(key);
        }
    }
}