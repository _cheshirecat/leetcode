﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleLeetCode.QuickSort
{
    public class QuickSort
    {
        public int[] Sort(int[] array)
        {
            if (array.Length < 2)
            {
                return array;
            }

            if (array.Length == 2)
            {
                if (array[0] > array[1])
                {
                    var temp = array[0];
                    array[0] = array[1];
                    array[1] = temp;
                }
            }

            var pivot = array[0];
            var lessValues = array.Skip(1).Where(i => i <= pivot).ToArray();
            var greaterValues = array.Skip(1).Where(i => i > pivot).ToArray();

            var newArray = new int[array.Length];
            Sort(lessValues).CopyTo(newArray, 0);
            new int[] { pivot }.CopyTo(newArray, lessValues.Length);
            Sort(greaterValues).CopyTo(newArray, lessValues.Length + 1);

            return newArray;
        }
    }
}
