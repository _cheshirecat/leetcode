﻿using System;
using System.Collections.Generic;

namespace GoogleLeetCode.QueueReconstructionByHeight
{
    public class Solution
    {
        public int[][] ReconstructQueue(int[][] people)
        {
            var heightIndex = 0;
            var peopleInFrontIndex = 1;
            var res = new List<int[]>();

            Array.Sort(people,
                    (person1, person2) =>
                    person1[heightIndex] == person2[heightIndex] ?
                    person1[peopleInFrontIndex] - person2[peopleInFrontIndex] :
                    person2[heightIndex] - person1[heightIndex]
            );

            foreach (var person in people)
            {
                res.Insert(person[peopleInFrontIndex], person);
            }

            return res.ToArray();
        }
    }
}
