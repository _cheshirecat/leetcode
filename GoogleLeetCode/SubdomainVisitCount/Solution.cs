﻿using System.Collections.Generic;
using System.Linq;

namespace GoogleLeetCode.SubdomainVisitCount
{
    public class Solution
    {
        private DomainExtractor domainExtractor;

        public Solution()
        {
            domainExtractor = new DomainExtractor();
        }

        public IList<string> SubdomainVisits(string[] cpdomains)
        {
            var dict = new Dictionary<string, int>();

            for (var i = 0; i < cpdomains.Length; i++)
            {
                var domainToProcess = cpdomains[i];
                var indexOfSpace = domainToProcess.IndexOf(' ');
                var number = int.Parse(domainToProcess.Substring(0, indexOfSpace));
                var domain = domainToProcess.Substring(indexOfSpace + 1);
                var subDomains = domainExtractor.ExtractDomains(domain);

                for (var j = 0; j < subDomains.Count; j++)
                {
                    var subDomain = subDomains[j];
                    var currentNumber = dict.ContainsKey(subDomain) ? (dict[subDomain] + number) : number;
                    dict[subDomain] = currentNumber;
                }
            }

            return dict.Select(kvp => string.Format("{0} {1}", kvp.Value, kvp.Key)).ToList();
        }
    }

    public class DomainExtractor
    {
        private char dot;

        public DomainExtractor()
        {
            dot = '.';
        }

        public List<string> ExtractDomains(string fromInput)
        {
            var parts = fromInput.Split(new char[] { dot });
            var startIndex = parts.Length - 1;
            var part = string.Empty;
            var res = new List<string>();

            for (var i = startIndex; i >= 0; i--)
            {
                var currentPart = parts[i];
                var domain = (i == startIndex) ? currentPart : string.Format("{0}{1}{2}", currentPart, dot, part);
                part = domain;

                res.Add(domain);
            }

            return res;
        }
    }
}