﻿using System.Collections.Generic;

namespace GoogleLeetCode.FizzBuzz
{
    public class Solution
    {
        public IList<string> FizzBuzz(int n)
        {
            var res = new List<string>();
            var threeCounter = 3;
            var fiveCounter = 5;
            var fifteenCounter = 15;

            for (var i = 1; i <= n; i++)
            {
                threeCounter--;
                fiveCounter--;
                fifteenCounter--;

                if (fifteenCounter == 0)
                {
                    threeCounter = 3;
                    fiveCounter = 5;
                    fifteenCounter = 15;
                    res.Add("FizzBuzz");
                }
                else if (fiveCounter == 0)
                {
                    fiveCounter = 5;
                    res.Add("Buzz");
                }
                else if (threeCounter == 0)
                {
                    threeCounter = 3;
                    res.Add("Fizz");
                }
                else
                {
                    res.Add(i.ToString());
                }
            }

            return res;
        }
    }
}