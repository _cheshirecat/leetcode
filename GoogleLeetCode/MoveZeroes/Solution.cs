﻿using System.Linq;

namespace GoogleLeetCode.MoveZeroes
{
    public class Solution
    {
        public void MoveZeroes(int[] nums)
        {
            var nonZeros = nums.Where(n => n != 0);
            var index = 0;

            foreach (var nonZero in nonZeros)
            {
                nums[index] = nonZero;
                index++;
            }

            for (var i = index; i < nums.Length; i++)
            {
                nums[i] = 0;
            }
        }
    }
}