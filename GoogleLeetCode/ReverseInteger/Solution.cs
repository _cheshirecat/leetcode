﻿using System.Collections.Generic;

namespace GoogleLeetCode.ReverseInteger
{
    public class Solution
    {
        public int Reverse(int x)
        {
            var inputAsString = x.ToString();
            var multiplier = 1;
            var output = new List<char>();
            var lastIndex = inputAsString.Length - 1;

            for (var i = lastIndex; i > -1; i--)
            {
                var currentChar = inputAsString[i];
                var isFirstChar = (i == lastIndex);
                if (isFirstChar && currentChar == '0')
                {
                    continue;
                }

                var isLastChar = (i == 0);
                if (isLastChar && currentChar == '-')
                {
                    multiplier = -1;
                    break;
                }

                output.Add(currentChar);
            }

            var resAsString = new string(output.ToArray());
            var res = 0;

            if (int.TryParse(resAsString, out res))
            {
                res = res * multiplier;
            }

            return res;
        }
    }
}