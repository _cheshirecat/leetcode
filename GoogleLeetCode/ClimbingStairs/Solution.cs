﻿namespace GoogleLeetCode.ClimbingStairs
{
    public class Solution
    {
        public int ClimbStairs(int n)
        {
            if (n == 1)
            {
                return 1;
            }

            var prePreviousSteps = 1;
            var previousSteps = 1;
            var currentSteps = 0;

            for (var i = 2; i <= n; i++)
            {
                currentSteps = prePreviousSteps + previousSteps;
                prePreviousSteps = previousSteps;
                previousSteps = currentSteps;
            }

            return currentSteps;
        }
    }
}