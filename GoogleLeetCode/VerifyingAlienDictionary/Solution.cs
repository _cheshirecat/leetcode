﻿using System.Collections.Generic;

namespace GoogleLeetCode.VerifyingAlienDictionary
{
    public class Solution
    {
        public bool IsAlienSorted(string[] words, string order)
        {
            var dictionary = new AlienDictionary(order);
            var wordsComparer = new WordsComparer(dictionary);
            var numberOfPairs = words.Length - 1;
            var wordIndex1 = 0;
            var wordIndex2 = 1;

            for (var i = 0; i < numberOfPairs; i++)
            {
                var word1 = words[wordIndex1];
                var word2 = words[wordIndex2];
                var rightOrder = wordsComparer.IsRightOrder(word1, word2);

                if (!rightOrder)
                {
                    return false;
                }

                wordIndex1++;
                wordIndex2++;
            }

            return true;
        }
    }

    public class AlienDictionary
    {
        private Dictionary<char, int> dict;

        public AlienDictionary(string order)
        {
            dict = new Dictionary<char, int>();

            for (var i = 0; i < order.Length; i++)
            {
                var key = order[i];
                dict[key] = i;
            }
        }

        public int Index(char ofChar)
        {
            return dict.ContainsKey(ofChar) ? dict[ofChar] : -1;
        }
    }

    public class WordsComparer
    {
        private AlienDictionary dictionary;

        public WordsComparer(AlienDictionary dictionary)
        {
            this.dictionary = dictionary;
        }

        public bool IsRightOrder(string word1, string word2)
        {
            var maxLength = word1.Length > word2.Length ? word1.Length : word2.Length;

            for (var i = 0; i < maxLength; i++)
            {
                var indexOfChar1 = GetIndex(word1, i);
                var indexOfChar2 = GetIndex(word2, i);

                if (indexOfChar1 == indexOfChar2)
                {
                    continue;
                }

                return indexOfChar1 < indexOfChar2;
            }

            return false;
        }

        public int GetIndex(string word, int index)
        {
            if (index < word.Length)
            {
                var charFromWord = word[index];
                var indexOfChar = dictionary.Index(charFromWord);

                return indexOfChar;
            }

            return -1;
        }
    }
}