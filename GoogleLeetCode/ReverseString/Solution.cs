﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleLeetCode.ReverseString
{
    public class Solution
    {
        public void ReverseString(char[] s)
        {
            var halfLength = s.Length / 2;
            var endIndex = s.Length - 1;

            for (var startIndex = 0; startIndex < halfLength; startIndex++)
            {
                var temp = s[startIndex];
                s[startIndex] = s[endIndex];
                s[endIndex] = temp;
                endIndex--;
            }
        }
    }
}