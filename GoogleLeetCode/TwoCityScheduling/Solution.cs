﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleLeetCode.TwoCityScheduling
{
    public class Solution
    {
        public int TwoCitySchedCost(int[][] costs)
        {
            int half = costs.Length / 2;
            var costOrderedByDifference = costs.OrderByDescending(c => c[0] - c[1]).ToArray();
            var res = 0;

            for (int i = 0; i < half; i++)
            {
                res += costOrderedByDifference[i][1];
            }

            for (int i = half; i < costs.Length; i++)
            {
                res += costOrderedByDifference[i][0];
            }


            return res;
        }
    }
}