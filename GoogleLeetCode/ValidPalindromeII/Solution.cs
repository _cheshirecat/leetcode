﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleLeetCode.ValidPalindromeII
{
    public class Solution
    {
        public bool ValidPalindrome(string s)
        {
            var halfLength = s.Length / 2;
            var startIndex = 0;
            var endIndex = s.Length - 1;
            var sames = 0;

            while (halfLength > 0)
            {
                if (s[startIndex] == s[endIndex])
                {
                    sames++;
                }
                else
                {
                    break;
                }

                startIndex++;
                endIndex--;
                halfLength--;
            }

            if ((s.Length == sames * 2) || (s.Length == sames * 2 + 1))
            {
                return true;
            }

            var startIndex1 = startIndex + 1;
            var endIndex1 = endIndex;

            while (halfLength > 0)
            {
                if (s[startIndex1] == s[endIndex1])
                {
                    sames++;
                }
                else
                {
                    break;
                }

                startIndex1++;
                endIndex1--;
                halfLength--;
            }

            if ((s.Length == sames * 2) || (s.Length == sames * 2 + 1))
            {
                return true;
            }

            var startIndex2 = startIndex1 - 1;
            var endIndex2 = endIndex1 - 1;

            while (halfLength > 0)
            {
                if (s[startIndex2] == s[endIndex2])
                {
                    sames++;
                }
                else
                {
                    break;
                }

                startIndex2++;
                endIndex2--;
                halfLength--;
            }

            return (s.Length == sames * 2) || (s.Length == sames * 2 + 1);
        }
    }
}
