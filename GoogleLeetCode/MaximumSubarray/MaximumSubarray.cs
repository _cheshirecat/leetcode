﻿using System;
using System.Linq;

namespace GoogleLeetCode.MaximumSubarray
{
    public class Solution
    {
        public int MaxSubArray(int[] nums)
        {
            if (nums == null)
            {
                return 0;
            }

            if (nums.Length == 0)
            {
                return 0;
            }

            var inputLength = nums.Length;
            var matrix = new int[inputLength, inputLength];
            var res = int.MinValue;

            for (var i = 0; i < inputLength; i++)
            {
                matrix[i, i] = nums[i];
                if (matrix[i, i] > res)
                {
                    res = matrix[i, i];
                }
            }

            for (var endIndex = 1; endIndex < inputLength; endIndex++)
            {
                var startIndex = endIndex - 1;
                matrix[startIndex, endIndex] = nums[startIndex] + nums[endIndex];

                if (matrix[startIndex, endIndex] > res)
                {
                    res = matrix[startIndex, endIndex];
                }
            }

            var countOfRemainElements = inputLength - 2; // 9 - 2 = 7
            var countOfPairs = inputLength - 2;
            var stepForward = 2;
            for (var i = 0; i < countOfRemainElements; i++)
            {
                for (var startIndex = 0; startIndex < countOfPairs; startIndex++)
                {
                    var endIndex = startIndex + stepForward;
                    matrix[startIndex, endIndex] = nums[startIndex] + nums[endIndex] + matrix[startIndex + 1, endIndex - 1];

                    if (matrix[startIndex, endIndex] > res)
                    {
                        res = matrix[startIndex, endIndex];
                    }
                }

                countOfPairs--;
                stepForward++;
            }

            return res;
        }
    }
}