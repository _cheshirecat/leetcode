﻿using System.Collections.Generic;

namespace GoogleLeetCode.ReverseLinkedList
{
    public class Solution
    {
        public ListNode ReverseList(ListNode head)
        {
            if (head == null)
            {
                return head;
            }

            var list = new List<ListNode>();
            var currentNode = head;

            while (currentNode != null)
            {
                list.Add(currentNode);
                currentNode = currentNode.next;
            }

            list[0].next = null;
            var listCount = list.Count;

            for (var i = 1; i < listCount; i++)
            {
                list[i].next = list[i - 1];
            }

            return list[listCount - 1];
        }
    }

    public class ListNode
    {
        public int val;
        public ListNode next;

        public ListNode(int val = 0, ListNode next = null)
        {
            this.val = val;
            this.next = next;
        }
    }
}