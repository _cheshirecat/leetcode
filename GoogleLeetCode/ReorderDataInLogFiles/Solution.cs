﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleLeetCode.ReorderDataInLogFiles
{
    public class Solution
    {
        private LogTypeResolver logTypeResolver;
        private LetterLogsSorter letterLogsSorter;

        public Solution()
        {
            logTypeResolver = new LogTypeResolver();
            letterLogsSorter = new LetterLogsSorter();
        }

        public string[] ReorderLogFiles(string[] logs)
        {
            var letterLogs = new List<string>();
            var digitLogs = new List<string>();

            for (var i = 0; i < logs.Length; i++)
            {
                var currentLog = logs[i];
                switch (logTypeResolver.GetLogType(currentLog))
                {
                    case LogType.DigitLog:
                        digitLogs.Add(currentLog);
                        break;
                    case LogType.LetterLog:
                        letterLogs.Add(currentLog);
                        break;
                }
            }

            var sortedLetterLogs = letterLogsSorter.GetSortedLogs(letterLogs);
            var res = new List<string>();

            res.AddRange(sortedLetterLogs);
            res.AddRange(digitLogs);

            return res.ToArray();
        }
    }

    public class LetterLogsSorter
    {
        private LetterLogComparer comparer;

        public LetterLogsSorter()
        {
            comparer = new LetterLogComparer();
        }

        public IEnumerable<string> GetSortedLogs(List<string> logs)
        {
            var letterLogs = logs
                .Select(log => new LetterLog(log))
                .OrderBy(letterLog => letterLog, comparer)
                .Select(letterLog => letterLog.Log);

            return letterLogs;
        }
    }

    public class LetterLogComparer : IComparer<LetterLog>
    {
        public int Compare(LetterLog log1, LetterLog log2)
        {
            var logValueComparison = string.Compare(log1.LogValue, log2.LogValue);

            if (logValueComparison < 0)
            {
                return -1;
            }
            else if (logValueComparison > 0)
            {
                return 1;
            }

            var logIdentifierComparison = string.Compare(log1.Log, log2.Log);

            if (logIdentifierComparison < 0)
            {
                return -1;
            }
            else if (logIdentifierComparison > 0)
            {
                return 1;
            }

            return 0;
        }
    }

    public class LetterLog
    {
        private string log;

        public string Log => log;

        public string LogValue
        {
            get
            {
                var firstSpaceIndex = log.IndexOf(' ');
                var logItself = log.Substring(firstSpaceIndex + 1);

                return logItself;
            }
        }

        public LetterLog(string log)
        {
            this.log = log;
        }
    }

    public class LogTypeResolver
    {
        public LogType GetLogType(string log)
        {
            var firstSpaceIndex = log.IndexOf(' ');
            var firstCharAfterSpace = log.Substring(firstSpaceIndex + 1, 1);

            int num;
            var isNumber = int.TryParse(firstCharAfterSpace, out num);

            return isNumber ? LogType.DigitLog : LogType.LetterLog;
        }
    }

    public enum LogType
    {
        LetterLog,
        DigitLog
    }
}