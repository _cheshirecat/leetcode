﻿using System.Collections.Generic;

namespace GoogleLeetCode.ValidParentheses
{
    public class Solution
    {
        private BracketTypesUtil bracketTypesUtil;

        public Solution()
        {
            bracketTypesUtil = new BracketTypesUtil();
        }

        public bool IsValid(string s)
        {
            if (s == null)
            {
                return false;
            }

            if (s.Length == 0)
            {
                return true;
            }

            if (s.Length % 2 != 0)
            {
                return false;
            }

            var firstBracket = s[0];
            var firstBracketIsClosing = bracketTypesUtil.GetBracketType(firstBracket);

            if (firstBracketIsClosing == BracketTypes.Closing)
            {
                return false;
            }

            var stack = new Stack<char>();

            for (var i = 0; i < s.Length; i++)
            {
                var currentBracket = s[i];
                var currentBracketType = bracketTypesUtil.GetBracketType(currentBracket);

                switch (currentBracketType)
                {
                    case BracketTypes.Opening:
                        stack.Push(currentBracket);
                        break;
                    case BracketTypes.Closing:
                        var bracketOnTheTopOfStack = stack.Count == 0
                            ? '#'
                            : stack.Pop();

                        if (bracketOnTheTopOfStack != bracketTypesUtil.GetOpeningBracket(currentBracket))
                        {
                            return false;
                        }
                        break;
                    default:
                        break;
                }
            }

            return stack.Count == 0;
        }
    }

    public class BracketTypesUtil
    {
        private Dictionary<char, char> bracketPairs;

        public BracketTypesUtil()
        {
            bracketPairs = new Dictionary<char, char>
            {
                { '}', '{' },
                { ']', '[' },
                { ')', '(' }
            };
        }

        public BracketTypes GetBracketType(char bracketToCheck)
        {
            return bracketPairs.ContainsKey(bracketToCheck)
                ? BracketTypes.Closing
                : BracketTypes.Opening;
        }

        public char GetOpeningBracket(char byClosingBracket)
        {
            return bracketPairs[byClosingBracket];
        }
    }

    public enum BracketTypes
    {
        Opening,
        Closing
    }
}