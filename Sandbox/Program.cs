﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GoogleLeetCode.QuickSort;
using GoogleLeetCode.ThreeSum;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            //[-1, 0, 1, 2, -1, -4]
            var s = new Solution();
            var array = new int[] { -1, 0, 1, 2, -1, -4 };
            var sorted = s.ThreeSum(array);
        }
    }
}
