﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GoogleLeetCode.RandomPickWithWeight;

namespace GoogleLeetCodeTests.RandomPickWithWeight
{
    [TestFixture]
    public class RandomPickWithWeightTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var w = new int[] { 1, 9, 15 };
            var solution = new Solution(w);

            // act
            solution.PickIndex();

            // assert
        }
    }
}
