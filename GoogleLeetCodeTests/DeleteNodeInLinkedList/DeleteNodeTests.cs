﻿using FluentAssertions;
using GoogleLeetCode.DeleteNodeInLinkedList;
using NUnit.Framework;

namespace GoogleLeetCodeTests.DeleteNodeInLinkedList
{
    [TestFixture]
    public class DeleteNodeTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            var node4 = new ListNode(4);

            var node5 = new ListNode(5);
            node4.next = node5;

            var node1 = new ListNode(1);
            node5.next = node1;

            var node9 = new ListNode(9);
            node1.next = node9;

            // act
            solution.DeleteNode(node5);

            // assert
            node4.next.val.Should().Be(1);
        }
    }
}
