﻿using FluentAssertions;
using GoogleLeetCode.PlusOne;
using NUnit.Framework;

namespace GoogleLeetCodeTests.PlusOne
{
    [TestFixture]
    public class PlusOneTests
    {
        [Test]
        public void When_Foo1()
        {
            // arrange
            var solution = new Solution();
            var input = new int[] { 1, 2, 3 };

            // act
            var res = solution.PlusOne(input);

            // assert
            res.Length.Should().Be(3);
            res[0].Should().Be(1);
            res[1].Should().Be(2);
            res[2].Should().Be(4);
        }

        [Test]
        public void When_Foo2()
        {
            // arrange
            var solution = new Solution();
            var input = new int[] { 4, 3, 2, 1 };

            // act
            var res = solution.PlusOne(input);

            // assert
            res.Length.Should().Be(4);
            res[0].Should().Be(4);
            res[1].Should().Be(3);
            res[2].Should().Be(2);
            res[2].Should().Be(2);
        }

        [Test]
        public void When_Foo3()
        {
            // arrange
            var solution = new Solution();
            var input = new int[] { 9 };

            // act
            var res = solution.PlusOne(input);

            // assert
            res.Length.Should().Be(2);
            res[0].Should().Be(1);
            res[1].Should().Be(0);
        }

        [Test]
        public void When_Foo4()
        {
            // arrange
            var solution = new Solution();
            var input = new int[] { 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 };

            // act
            var res = solution.PlusOne(input);

            // assert
            res.Length.Should().Be(10);
            res[0].Should().Be(9);
            res[1].Should().Be(8);
            res[2].Should().Be(7);
            res[3].Should().Be(6);
            res[4].Should().Be(5);
            res[5].Should().Be(4);
            res[6].Should().Be(3);
            res[7].Should().Be(2);
            res[8].Should().Be(1);
            res[9].Should().Be(1);
        }
    }
}