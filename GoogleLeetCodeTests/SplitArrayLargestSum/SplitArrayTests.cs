﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GoogleLeetCode.SplitArrayLargestSum;
using FluentAssertions;

namespace GoogleLeetCodeTests.SplitArrayLargestSum
{
    [TestFixture]
    public class SplitArrayTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            var nums = new int[] { 7, 2, 5, 10, 8 };
            var m = 2;

            // act
            var res = solution.SplitArray(nums, m);

            // assert
            res.Should().Be(18);
        }
    }
}
// [7,2,5,10,8]

// 7 | 2,5,10,8     7 | 25  =   32
// 7,2 | 5,10,8     9 | 23  =   32
// 7,2,5 | 10,8     14 | 18 =   32
// 7,2,5,10 | 8     24 | 8  =   32