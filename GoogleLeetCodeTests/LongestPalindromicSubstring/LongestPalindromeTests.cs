﻿using FluentAssertions;
using GoogleLeetCode.LongestPalindromicSubstring;
using NUnit.Framework;

namespace GoogleLeetCodeTests.LongestPalindromicSubstring
{
    [TestFixture]
    public class LongestPalindromeTests
    {
        [Test]
        public void When_StringIsEmpty()
        {
            // arrange
            var solution = new Solution();
            var input = string.Empty;

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be(string.Empty);
        }

        [Test]
        public void When_SingleChapterString()
        {
            // arrange
            var solution = new Solution();
            var input = "j";

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be("j");
        }

        [Test]
        public void When_TwoDifferentChapterString()
        {
            // arrange
            var solution = new Solution();
            var input = "ac";

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be("a");
        }

        [Test]
        public void When_TwoSameChapterString()
        {
            // arrange
            var solution = new Solution();
            var input = "99";

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be("99");
        }

        [Test]
        public void When_MoreThanTwoChapterString_ContainsPalindromic1()
        {
            // arrange
            var solution = new Solution();
            var input = "aabbac";

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be("abba");
        }

        [Test]
        public void When_MoreThanTwoChapterString_ContainsPalindromic2()
        {
            // arrange
            var solution = new Solution();
            var input = "cbbd";

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be("bb");
        }

        [Test]
        public void When_MoreThanTwoChapterString_DoesNot_ContainsPalindromic()
        {
            // arrange
            var solution = new Solution();
            var input = "abcda";

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be("a");
        }

        [Test]
        public void When_Long1String()
        {
            // arrange
            var solution = new Solution();
            var input = "civilwartestingwhetherthatnaptionoranynartionsoconceivedandsodedicatedcanlongendureWeareqmetonagreatbattlefiemldoftzhatwarWehavecometodedicpateaportionofthatfieldasafinalrestingplaceforthosewhoheregavetheirlivesthatthatnationmightliveItisaltogetherfangandproperthatweshoulddothisButinalargersensewecannotdedicatewecannotconsecratewecannothallowthisgroundThebravelmenlivinganddeadwhostruggledherehaveconsecrateditfaraboveourpoorponwertoaddordetractTgheworldadswfilllittlenotlenorlongrememberwhatwesayherebutitcanneverforgetwhattheydidhereItisforusthelivingrathertobededicatedheretotheulnfinishedworkwhichtheywhofoughtherehavethusfarsonoblyadvancedItisratherforustobeherededicatedtothegreattdafskremainingbeforeusthatfromthesehonoreddeadwetakeincreaseddevotiontothatcauseforwhichtheygavethelastpfullmeasureofdevotionthatweherehighlyresolvethatthesedeadshallnothavediedinvainthatthisnationunsderGodshallhaveanewbirthoffreedomandthatgovernmentofthepeoplebythepeopleforthepeopleshallnotperishfromtheearth";

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be("ranynar");
        }

        [Test]
        public void When_Long2String()
        {
            // arrange
            var solution = new Solution();
            var input = "vaomxdtiuwqlwhgutkhxxhccsgvyoaccuicgybnqnslogtqhblegfudagpxfvjdacsxgevvepuwthdtybgflsxjdmmfumyqgpxatvdypjmlapccaxwkuxkilqqgpihyepkilhlfkdrbsefinitdcaghqmhylnixidrygdnzmgubeybczjceiybowglkywrpkfcwpsjbkcpnvfbxnpuqzhotzspgebptnhwevbkcueyzecdrjpbpxemagnwmtwikmkpqluwmvyswvxghajknjxfazshsvjkstkezdlbnkwxawlwkqnxghjzyigkvqpapvsntojnxlmtywdrommoltpbvxwqyijpkirvndwpgufgjelqvwffpuycqfwenhzrbzbdtupyutgccdjyvhptnuhxdwbmdcbpfvxvtfryszhaakwshrjseonfvjrrdefyxefqfvadlwmedpvnozobftnnsutegrtxhwitrwdpfienhdbvvykoynrsbpmzjtotjxbvemgoxreiveakmmbbvbmfbbnyfxwrueswdlxvuelbkrdxlutyukppkzjnmfmclqpkwzyylwlzsvriwomchzzqwqglpflaepoxcnnewzstvegyaowwhgvcwjhbbstvzhhvghigoazbjiikglbqlxlccrwqvyqxpbtpoqjliziwmdkzfsrqtqdkeniulsavsfqsjwnvpprvczcujihoqeanobhlsvbzmgflhykndfydbxatskf";

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be("jtotj");
        }

        [Test]
        public void When_Long3String()
        {
            // arrange
            var solution = new Solution();
            var input = "gphyvqruxjmwhonjjrgumxjhfyupajxbjgthzdvrdqmdouuukeaxhjumkmmhdglqrrohydrmbvtuwstgkobyzjjtdtjroqpyusfsbjlusekghtfbdctvgmqzeybnwzlhdnhwzptgkzmujfldoiejmvxnorvbiubfflygrkedyirienybosqzrkbpcfidvkkafftgzwrcitqizelhfsruwmtrgaocjcyxdkovtdennrkmxwpdsxpxuarhgusizmwakrmhdwcgvfljhzcskclgrvvbrkesojyhofwqiwhiupujmkcvlywjtmbncurxxmpdskupyvvweuhbsnanzfioirecfxvmgcpwrpmbhmkdtckhvbxnsbcifhqwjjczfokovpqyjmbywtpaqcfjowxnmtirdsfeujyogbzjnjcmqyzciwjqxxgrxblvqbutqittroqadqlsdzihngpfpjovbkpeveidjpfjktavvwurqrgqdomiibfgqxwybcyovysydxyyymmiuwovnevzsjisdwgkcbsookbarezbhnwyqthcvzyodbcwjptvigcphawzxouixhbpezzirbhvomqhxkfdbokblqmrhhioyqubpyqhjrnwhjxsrodtblqxkhezubprqftrqcyrzwywqrgockioqdmzuqjkpmsyohtlcnesbgzqhkalwixfcgyeqdzhnnlzawrdgskurcxfbekbspupbduxqxjeczpmdvssikbivjhinaopbabrmvscthvoqqbkgekcgyrelxkwoawpbrcbszelnxlyikbulgmlwyffurimlfxurjsbzgddxbgqpcdsuutfiivjbyqzhprdqhahpgenjkbiukurvdwapuewrbehczrtswubthodv";

            // act
            var res = solution.LongestPalindrome(input);

            // assert
            res.Should().Be("jtdtj");
        }
    }
}