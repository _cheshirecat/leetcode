﻿using FluentAssertions;
using GoogleLeetCode.MoveZeroes;
using NUnit.Framework;

namespace GoogleLeetCodeTests.MoveZeroes
{
    [TestFixture]
    public class MoveZeroesTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            var nums = new int[] { 0, 1, 0, 3, 12 };

            // act
            solution.MoveZeroes(nums);

            // assert
            nums[0].Should().Be(1);
            nums[1].Should().Be(3);
            nums[2].Should().Be(12);
            nums[3].Should().Be(0);
            nums[4].Should().Be(0);
        }

        [Test]
        public void When_All_Nums_Are_Zero()
        {
            // arrange
            var solution = new Solution();
            var nums = new int[] { 0, 0, 0 };

            // act
            solution.MoveZeroes(nums);

            // assert
            nums[0].Should().Be(0);
            nums[1].Should().Be(0);
            nums[2].Should().Be(0);
        }

        [Test]
        public void When_Little_Difficult()
        {
            // arrange
            var solution = new Solution();
            var nums = new int[] { 4, 2, 4, 0, 0, 3, 0, 5, 1, 0 };

            // act
            solution.MoveZeroes(nums);

            // assert
            nums[0].Should().Be(4);
            nums[1].Should().Be(2);
            nums[2].Should().Be(4);
            nums[3].Should().Be(3);
            nums[4].Should().Be(5);
            nums[5].Should().Be(1);
            nums[6].Should().Be(0);
            nums[7].Should().Be(0);
            nums[8].Should().Be(0);
            nums[9].Should().Be(0);
        }
    }
}