﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GoogleLeetCode.MergeTwoSortedLists;
using FluentAssertions;

namespace GoogleLeetCodeTests.MergeTwoSortedLists
{
    [TestFixture]
    public class MergeTwoListsTests
    {
        [Test]
        public void When_FirstListIsNull()
        {
            // arrange
            var solution = new Solution();
            ListNode l1 = null;
            ListNode l2 = new ListNode();

            // act
            var res = solution.MergeTwoLists(l1, l2);

            // assert
            res.Should().Be(l2);
        }

        [Test]
        public void When_SecondListIsNull()
        {
            // arrange
            var solution = new Solution();
            ListNode l1 = new ListNode();
            ListNode l2 = null;

            // act
            var res = solution.MergeTwoLists(l1, l2);

            // assert
            res.Should().Be(l1);
        }

        [Test]
        public void When_Foo()
        {
            // arrange 
            var solution = new Solution();
            var ln1_3 = new ListNode(4);
            var ln1_2 = new ListNode(2, ln1_3);
            var ln1_1 = new ListNode(1, ln1_2);

            var ln2_3 = new ListNode(4);
            var ln2_2 = new ListNode(3, ln2_3);
            var ln2_1 = new ListNode(1, ln2_2);

            // act
            var res = solution.MergeTwoLists(ln1_1, ln2_1);

            //aseert
            res.val.Should().Be(1);
            res.next.val.Should().Be(1);
            res.next.next.val.Should().Be(2);
            res.next.next.next.val.Should().Be(3);
            res.next.next.next.next.val.Should().Be(4);
            res.next.next.next.next.next.val.Should().Be(4);
        }
    }
}