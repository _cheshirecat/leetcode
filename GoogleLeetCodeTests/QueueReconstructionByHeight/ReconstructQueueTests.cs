﻿using FluentAssertions;
using GoogleLeetCode.QueueReconstructionByHeight;
using NUnit.Framework;

namespace GoogleLeetCodeTests.QueueReconstructionByHeight
{
    [TestFixture]
    public class ReconstructQueueTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            int[][] people =
            {
                new int[] { 7, 0 },
                new int[] { 4, 4 },
                new int[] { 7, 1 },
                new int[] { 5, 0 },
                new int[] { 6, 1 },
                new int[] { 5, 2 }
            };

            // act
            var res = solution.ReconstructQueue(people);

            // assert
            res[0][0].Should().Be(5);
            res[0][1].Should().Be(0);

            res[1][0].Should().Be(7);
            res[1][1].Should().Be(0);

            res[2][0].Should().Be(5);
            res[2][1].Should().Be(2);

            res[3][0].Should().Be(6);
            res[3][1].Should().Be(1);

            res[4][0].Should().Be(4);
            res[4][1].Should().Be(4);

            res[5][0].Should().Be(7);
            res[5][1].Should().Be(1);
        }
    }
}