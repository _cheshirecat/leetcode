﻿using FluentAssertions;
using GoogleLeetCode.VerifyingAlienDictionary;
using NUnit.Framework;

namespace GoogleLeetCodeTests.VerifyingAlienDictionary
{
    [TestFixture]
    public class IsAlienSortedTests
    {
        [Test]
        public void When_Foo1()
        {
            // arrange
            var solution = new Solution();
            var words = new string[]
            {
                "hello", "helloworld", "leetcode", "brother", "zoo"
            };
            var order = "hlabcdefgijkmnopqrstuvwxyz";

            // act
            var res = solution.IsAlienSorted(words, order);

            // assert
            res.Should().BeTrue();
        }

        [Test]
        public void When_Foo2()
        {
            // arrange
            var solution = new Solution();
            var words = new string[]
            {
                "hello", "leetcode"
            };
            var order = "hlabcdefgijkmnopqrstuvwxyz";

            // act
            var res = solution.IsAlienSorted(words, order);

            // assert
            res.Should().BeTrue();
        }

        [Test]
        public void When_Foo3()
        {
            // arrange
            var solution = new Solution();
            var words = new string[]
            {
                "word", "world", "row"
            };
            var order = "worldabcefghijkmnpqstuvxyz";

            // act
            var res = solution.IsAlienSorted(words, order);

            // assert
            res.Should().BeFalse();
        }

        [Test]
        public void When_Foo4()
        {
            // arrange
            var solution = new Solution();
            var words = new string[]
            {
                "apple", "app"
            };
            var order = "abcdefghijklmnopqrstuvwxyz";

            // act
            var res = solution.IsAlienSorted(words, order);

            // assert
            res.Should().BeFalse();
        }
    }
}