﻿using FluentAssertions;
using GoogleLeetCode.ValidParentheses;
using NUnit.Framework;

namespace GoogleLeetCodeTests.ValidParentheses
{
    [TestFixture]
    public class ValidParenthesesTests
    {
        [Test]
        public void When_InputIsNull()
        {
            // arrange
            var solution = new Solution();

            // act
            var res = solution.IsValid(null);

            // assert
            res.Should().BeFalse();
        }

        [Test]
        public void When_InputIsEmpty()
        {
            // arrange
            var solution = new Solution();

            // act
            var res = solution.IsValid(string.Empty);

            // assert
            res.Should().BeTrue();
        }

        [Test]
        public void When_InputContainsOddNumberOfCharacters()
        {
            // arrange
            var solution = new Solution();
            var input = "{]}";

            // act
            var res = solution.IsValid(input);

            // assert
            res.Should().BeFalse();
        }

        [Test]
        public void When_InputStartsWithClosingBracket()
        {
            // arrange
            var solution = new Solution();
            var input = "){";

            // act
            var res = solution.IsValid(input);

            // assert
            res.Should().BeFalse();
        }

        [Test]
        public void When_InputIsValid1()
        {
            // arrange
            var solution = new Solution();
            var input = "{[]}";

            // act
            var res = solution.IsValid(input);

            // assert
            res.Should().BeTrue();
        }

        [Test]
        public void When_InputIsValid2()
        {
            // arrange
            var solution = new Solution();
            var input = "[[][{([])}[[[]{{{[()]}}}]]]]";

            // act
            var res = solution.IsValid(input);

            // assert
            res.Should().BeTrue();
        }
    }
}