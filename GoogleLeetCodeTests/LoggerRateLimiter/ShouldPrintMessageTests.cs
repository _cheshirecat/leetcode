﻿using FluentAssertions;
using GoogleLeetCode.LoggerRateLimiter;
using NUnit.Framework;

namespace GoogleLeetCodeTests.LoggerRateLimiter
{
    [TestFixture]
    public class ShouldPrintMessageTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var logger = new Logger();

            // act
            var res_1_foo = logger.ShouldPrintMessage(1, "foo");
            var res_2_bar = logger.ShouldPrintMessage(2, "bar");
            var res_3_foo = logger.ShouldPrintMessage(3, "foo");
            var res_8_bar = logger.ShouldPrintMessage(8, "bar");
            var res_10_foo = logger.ShouldPrintMessage(10, "foo");
            var res_11_foo = logger.ShouldPrintMessage(11, "foo");

            // asset
            res_1_foo.Should().BeTrue();
            res_2_bar.Should().BeTrue();
            res_3_foo.Should().BeFalse();
            res_8_bar.Should().BeFalse();
            res_10_foo.Should().BeFalse();
            res_11_foo.Should().BeTrue();
        }

        [Test]
        public void When_Foo_1()
        {
            // arrange
            var logger = new Logger();

            // act
            var res_100_bug = logger.ShouldPrintMessage(100, "bug");
            var res_109_bug = logger.ShouldPrintMessage(109, "bug");
            var res_109_bug_1 = logger.ShouldPrintMessage(109, "bug");

            // [[100,"bug"],[109,"bug"],[109,"bug"]]
            // -> [true,true,false]
            // <- [true,false,false]


            // asset
            res_100_bug.Should().BeTrue();
            res_109_bug.Should().BeFalse();
            res_109_bug_1.Should().BeFalse();
        }
    }
}