﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GoogleLeetCode.TwoCityScheduling;
using FluentAssertions;

namespace GoogleLeetCodeTests.TwoCityScheduling
{
    [TestFixture]
    public class TwoCitySchedCostTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            int[][] costs = new int[][]
            {
                new int[] { 10, 20 },   // -10      2   10(A)
                new int[] { 30, 200 },  // -70      3   30(A)
                new int[] { 400, 50 },  // 350      0   50(B)
                new int[] { 30, 20 }    // 10       1   20(B)
            };

            // act
            var cost = solution.TwoCitySchedCost(costs);

            // assert
            cost.Should().Be(110);
        }
    }
}
