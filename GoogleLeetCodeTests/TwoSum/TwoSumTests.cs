﻿using FluentAssertions;
using GoogleLeetCode.TwoSum;
using NUnit.Framework;

namespace GoogleLeetCodeTests.TwoSum
{
    [TestFixture]
    public class TwoSumTests
    {
        [Test]
        public void When_InputIsNull()
        {
            // arrange
            var solution = new Solution();
            int[] input = null;

            // act
            var res = solution.TwoSum(input, 0);

            // assert
            res.Should().BeEmpty();
        }

        [Test]
        public void When_InputIsEmpty()
        {
            // arrange
            var solution = new Solution();
            int[] input = new int[] { };

            // act
            var res = solution.TwoSum(input, 0);

            // assert
            res.Should().BeEmpty();
        }

        [Test]
        public void When_InputContainsSingleInt()
        {
            // arrange
            var solution = new Solution();
            int[] input = new int[] { 7 };

            // act
            var res = solution.TwoSum(input, 0);

            // assert
            res.Should().BeEmpty();
        }

        [Test]
        public void When_InputFoo()
        {
            // arrange
            var solution = new Solution();
            var input = new int[] { 2, 7, 11, 15 };
            var target = 9;

            // act
            var res = solution.TwoSum(input, target);

            // assert
            res.Should().NotBeEmpty();
            res.Length.Should().Be(2);
            res[0].Should().Be(0);
            res[1].Should().Be(1);
        }
    }
}
