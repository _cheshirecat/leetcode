﻿using FluentAssertions;
using GoogleLeetCode.BestTimeToBuyAndSellStock;
using NUnit.Framework;

namespace GoogleLeetCodeTests.BestTimeToBuyAndSellStock
{
    [TestFixture]
    public class MaxProfitTests
    {
        [Test]
        public void When_TransactionExists()
        {
            // arrange
            var solution = new Solution();
            var input = new int[]
            {
                7, 1, 5, 3, 6, 4
            };

            // act
            var res = solution.MaxProfit(input);

            // assert
            res.Should().Be(5);
        }

        [Test]
        public void When_TransactionDoesNotExist()
        {
            // arrange
            var solution = new Solution();
            var input = new int[]
            {
                7, 6, 4, 3, 1
            };

            // act
            var res = solution.MaxProfit(input);

            // assert
            res.Should().Be(0);
        }
    }
}