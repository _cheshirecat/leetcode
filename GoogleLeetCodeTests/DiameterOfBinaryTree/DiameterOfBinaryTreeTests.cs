﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GoogleLeetCode.DiameterOfBinaryTree;
using FluentAssertions;

namespace GoogleLeetCodeTests.DiameterOfBinaryTree
{
    [TestFixture]
    public class DiameterOfBinaryTreeTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();

            var node4 = new TreeNode(4);
            var node5 = new TreeNode(5);
            var node2 = new TreeNode(2, node4, node5);

            var node3 = new TreeNode(3);

            var node1 = new TreeNode(1, node2, node3);

            // act
            var res = solution.DiameterOfBinaryTree(node1);

            // assert
            res.Should().Be(3);
        }
    }
}

/*
          1
         / \
        2   3
       / \     
      4   5
*/