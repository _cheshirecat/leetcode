﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GoogleLeetCode.ReorderDataInLogFiles;
using FluentAssertions;

namespace GoogleLeetCodeTests.ReorderDataInLogFiles
{
    [TestFixture]
    public class ReorderLogFilesTests
    {
        [Test]
        public void When_Foo1()
        {
            // arrange
            var solution = new Solution();
            var input = new string[]
            {
                "dig1 8 1 5 1",
                "let1 art can",
                "dig2 3 6",
                "let2 own kit dig",
                "let3 art zero"
            };

            // act
            var res = solution.ReorderLogFiles(input);

            // assert
            res[0].Should().Be("let1 art can");
            res[1].Should().Be("let3 art zero");
            res[2].Should().Be("let2 own kit dig");
            res[3].Should().Be("dig1 8 1 5 1");
            res[4].Should().Be("dig2 3 6");
        }

        [Test]
        public void When_Foo2()
        {
            // arrange
            var solution = new Solution();
            var input = new string[]
            {
                "a1 9 2 3 1",
                "g1 act car",
                "zo4 4 7",
                "ab1 off key dog",
                "a8 act zoo",
                "a2 act car"
            };

            // act
            var res = solution.ReorderLogFiles(input);

            // assert
            res[0].Should().Be("a2 act car");
            res[1].Should().Be("g1 act car");
            res[2].Should().Be("a8 act zoo");
            res[3].Should().Be("ab1 off key dog");
            res[4].Should().Be("a1 9 2 3 1");
            res[5].Should().Be("zo4 4 7");
        }
    }
}