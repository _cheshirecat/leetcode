﻿using FluentAssertions;
using GoogleLeetCode.ReverseLinkedList;
using NUnit.Framework;

namespace GoogleLeetCodeTests.ReverseLinkedList
{
    [TestFixture]
    public class ReverseListTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange 
            var node5 = new ListNode(5);
            var node4 = new ListNode(4, node5);
            var node3 = new ListNode(3, node4);
            var node2 = new ListNode(2, node3);
            var node1 = new ListNode(1, node2);
            var solution = new Solution();

            // act
            var res = solution.ReverseList(node1);

            // assert
            res.val.Should().Be(5);
            res.next.val.Should().Be(4);
            res.next.next.val.Should().Be(3);
            res.next.next.next.val.Should().Be(2);
            res.next.next.next.next.val.Should().Be(1);
        }
    }
}