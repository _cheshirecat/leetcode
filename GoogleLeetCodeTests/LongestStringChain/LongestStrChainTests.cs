﻿using System.Linq;
using FluentAssertions;
using GoogleLeetCode.LongestStringChain;
using NUnit.Framework;

namespace GoogleLeetCodeTests.LongestStringChain
{
    [TestFixture]
    public class LongestStrChainTests
    {
        [Test]
        public void When_Foo1()
        {
            // arrange
            var solution = new Solution();
            var words = new string[]
            {
                "a","b","ba","bca","bda","bdca"
            };

            // act
            var res = solution.LongestStrChain(words);

            // assert
            res.Should().Be(4);
        }

        [Test]
        public void When_Foo2()
        {
            // arrange
            var solution = new Solution();
            var words = new string[]
            {
                "ksqvsyq",
                "ks",
                "kss",
                "czvh",
                "zczpzvdhx",
                "zczpzvh",
                "zczpzvhx",
                "zcpzvh",
                "zczvh",
                "gr",
                "grukmj",
                "ksqvsq",
                "gruj",
                "kssq",
                "ksqsq",
                "grukkmj",
                "grukj",
                "zczpzfvdhx",
                "gru"
            };

            // act
            var res = solution.LongestStrChain(words);

            // assert
            res.Should().Be(4);
        }
    }
}