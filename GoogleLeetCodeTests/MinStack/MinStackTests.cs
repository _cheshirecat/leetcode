﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace GoogleLeetCodeTests.MinStack
{
    [TestFixture]
    public class MinStackTests
    {
        [Test]
        public void When_Foo1()
        {
            // arrange
            var minStack = new GoogleLeetCode.MinStack.MinStack();

            // act
            minStack.Push(-2);
            minStack.Push(0);
            minStack.Push(-3);

            // assert
            var min = minStack.GetMin(); // return -3
            min.Should().Be(-3);

            // act
            minStack.Pop();

            // assert
            var top = minStack.Top();    // return 0
            top.Should().Be(0);

            min = minStack.GetMin(); // return -2
            min.Should().Be(-2);
        }

        [Test]
        public void When_Foo2()
        {
            // arrange
            var minStack = new GoogleLeetCode.MinStack.MinStack();

            // act
            minStack.Push(0);
            minStack.Push(1);
            minStack.Push(0);

            // assert
            var min = minStack.GetMin();
            min.Should().Be(0);

            // act
            minStack.Pop();

            // assert
            min = minStack.GetMin();
            min.Should().Be(0);
        }
    }
}
