﻿using FluentAssertions;
using GoogleLeetCode.InvertBinaryTree;
using NUnit.Framework;

namespace GoogleLeetCodeTests.InvertBinaryTree
{
    [TestFixture]
    public class TreeNodeTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange 
            var solution = new Solution();

            var node1 = new TreeNode(1);
            var node3 = new TreeNode(3);
            var node2 = new TreeNode(2, node1, node3);

            var node6 = new TreeNode(6);
            var node9 = new TreeNode(9);
            var node7 = new TreeNode(7, node6, node9);

            var node4 = new TreeNode(4, node2, node7);

            // act
            node4 = solution.InvertTree(node4);

            // assert
            node4.left.Should().Be(node7);
            node4.right.Should().Be(node2);

            node7.left.Should().Be(node9);
            node7.right.Should().Be(node6);

            node2.left.Should().Be(node3);
            node2.right.Should().Be(node1);
        }
    }
}
