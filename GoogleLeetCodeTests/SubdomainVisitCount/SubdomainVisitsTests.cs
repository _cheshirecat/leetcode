﻿using System.Collections.Generic;
using FluentAssertions;
using GoogleLeetCode.SubdomainVisitCount;
using NUnit.Framework;

namespace GoogleLeetCodeTests.SubdomainVisitCount
{
    [TestFixture]
    public class SubdomainVisitsTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            string[] input = new string[]
            {
                "900 google.mail.com",
                "50 yahoo.com",
                "1 intel.mail.com",
                "5 wiki.org"
            };
            var expectedRes = new List<string>
            {
                "901 mail.com",
                "50 yahoo.com",
                "900 google.mail.com",
                "5 wiki.org",
                "5 org",
                "1 intel.mail.com",
                "951 com"
            };

            // act
            var res = solution.SubdomainVisits(input);

            // assert
            res.Should().Contain(expectedRes);
        }
    }

    [TestFixture]
    public class ExtractDomainsTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange 
            var extractor = new DomainExtractor();
            var input = "discuss.leetcode.com";
            var exteptedRes = new List<string>
            {
                "discuss.leetcode.com",
                "leetcode.com",
                "com"
            };

            // act
            var res = extractor.ExtractDomains(input);

            // assert
            res.Count.Should().Be(3);
            res.Should().Contain(exteptedRes);
        }
    }
}