﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GoogleLeetCode.ReverseString;
using FluentAssertions;

namespace GoogleLeetCodeTests.ReverseString
{
    [TestFixture]
    public class ReverseStringTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            var s = "qwerty".ToCharArray();

            // act
            solution.ReverseString(s);

            // assert
            s[0].Should().Be('y');
            s[1].Should().Be('t');
            s[2].Should().Be('r');
            s[3].Should().Be('e');
            s[4].Should().Be('w');
            s[5].Should().Be('q');
        }
    }
}