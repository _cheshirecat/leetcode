﻿using FluentAssertions;
using GoogleLeetCode.ReverseInteger;
using NUnit.Framework;

namespace GoogleLeetCodeTests.ReverseInteger
{
    [TestFixture]
    public class ReverseTests
    {
        [Test]
        public void When_1()
        {
            // arrange
            var solution = new Solution();
            int input = -1230;

            // act
            var res = solution.Reverse(input);

            // assert
            res.Should().Be(-321);
        }

        [Test]
        public void When_2()
        {
            // arrange
            var solution = new Solution();
            int input = 123;

            // act
            var res = solution.Reverse(input);

            // assert
            res.Should().Be(321);
        }

        [Test]
        public void When_3()
        {
            // arrange
            var solution = new Solution();
            int input = -123;

            // act
            var res = solution.Reverse(input);

            // assert
            res.Should().Be(-321);
        }

        [Test]
        public void When_4()
        {
            // arrange
            var solution = new Solution();
            int input = 120;

            // act
            var res = solution.Reverse(input);

            // assert
            res.Should().Be(21);
        }
    }
}