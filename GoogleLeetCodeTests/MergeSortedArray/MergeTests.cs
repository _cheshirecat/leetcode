﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GoogleLeetCode.MergeSortedArray;

namespace GoogleLeetCodeTests.MergeSortedArray
{
    [TestFixture]
    public class MergeTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            var nums1 = new int[] { 1, 2, 3, 0, 0, 0 };
            var m = 3;
            var nums2 = new int[] { 2, 5, 6 };
            var n = 3;

            // act
            solution.Merge(nums1, m, nums2, n);

            // assert
        }
    }
}
