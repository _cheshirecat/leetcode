﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using GoogleLeetCode.DesignHashMap;
using NUnit.Framework;

namespace GoogleLeetCodeTests.DesignHashMap
{
    [TestFixture]
    public class MyHashMapTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            MyHashMap hashMap = new MyHashMap();

            // act
            hashMap.Put(1, 1);
            hashMap.Put(2, 2);

            // assert
            hashMap.Get(1).Should().Be(1);
            hashMap.Get(3).Should().Be(-1);

            // act
            hashMap.Put(2, 1);

            // assert
            hashMap.Get(2).Should().Be(1);

            // act
            hashMap.Remove(2);

            // assert
            hashMap.Get(2);
        }
    }
}
/*
MyHashMap hashMap = new MyHashMap();
hashMap.put(1, 1);          
hashMap.put(2, 2);         
hashMap.get(1);            // returns 1
hashMap.get(3);            // returns -1 (not found)

hashMap.put(2, 1);          // update the existing value
hashMap.get(2);            // returns 1 

hashMap.remove(2);          // remove the mapping for 2
hashMap.get(2);            // returns -1 (not found) 
*/
