﻿using FluentAssertions;
using GoogleLeetCode.ValidPalindromeII;
using NUnit.Framework;

namespace GoogleLeetCodeTests.ValidPalindromeII
{
    [TestFixture]
    public class ValidPalindromeTests
    {
        [Test]
        public void When_aba()
        {
            // arrange
            var solution = new Solution();
            var s = "aba";

            // act
            var res = solution.ValidPalindrome(s);

            // assert
            res.Should().BeTrue();
        }

        [Test]
        public void When_abc()
        {
            // arrange
            var solution = new Solution();
            var s = "abc";

            // act
            var res = solution.ValidPalindrome(s);

            // assert
            res.Should().BeFalse();
        }

        [Test]
        public void When_abca()
        {
            // arrange
            var solution = new Solution();
            var s = "abca";

            // act
            var res = solution.ValidPalindrome(s);

            // assert
            res.Should().BeTrue();
        }

        [Test]
        public void When_abcdcbza()
        {
            // arrange
            var solution = new Solution();
            var s = "abcdcbza";

            // act 
            var res = solution.ValidPalindrome(s);

            // assert
            res.Should().BeTrue();
        }
    }
}
