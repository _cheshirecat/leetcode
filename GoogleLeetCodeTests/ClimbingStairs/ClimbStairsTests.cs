﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GoogleLeetCode.ClimbingStairs;
using FluentAssertions;

namespace GoogleLeetCodeTests.ClimbingStairs
{
    [TestFixture]
    public class ClimbStairsTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            var n = 5;

            // act
            var res = solution.ClimbStairs(n);

            // assert
            res.Should().Be(8);
        }
    }
}