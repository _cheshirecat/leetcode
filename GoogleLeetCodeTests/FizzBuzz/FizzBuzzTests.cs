﻿using System.Linq;
using FluentAssertions;
using GoogleLeetCode.FizzBuzz;
using NUnit.Framework;

namespace GoogleLeetCodeTests.FizzBuzz
{
    [TestFixture]
    public class FizzBuzzTests
    {
        [Test]
        public void When_Foo()
        {
            // arrange
            var solution = new Solution();
            var input = 15;

            // act
            var res = solution.FizzBuzz(input);

            // assert
            res.Count.Should().Be(15);
            res.ElementAt(0).Should().Be("1");
            res.ElementAt(1).Should().Be("2");
            res.ElementAt(2).Should().Be("Fizz");
            res.ElementAt(3).Should().Be("4");
            res.ElementAt(4).Should().Be("Buzz");
            res.ElementAt(5).Should().Be("Fizz");
            res.ElementAt(6).Should().Be("7");
            res.ElementAt(7).Should().Be("8");
            res.ElementAt(8).Should().Be("Fizz");
            res.ElementAt(9).Should().Be("Buzz");
            res.ElementAt(10).Should().Be("11");
            res.ElementAt(11).Should().Be("Fizz");
            res.ElementAt(12).Should().Be("13");
            res.ElementAt(13).Should().Be("14");
            res.ElementAt(14).Should().Be("FizzBuzz");
        }
    }
}