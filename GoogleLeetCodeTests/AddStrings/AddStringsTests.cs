﻿using FluentAssertions;
using GoogleLeetCode.AddStrings;
using NUnit.Framework;

namespace GoogleLeetCodeTests.AddStrings
{
    [TestFixture]
    public class AddStringsTests
    {
        [Test]
        public void When_Foo1()
        {
            // arrange 
            var solution = new Solution();
            var num1 =  "1234";
            var num2 = "29993";

            // act
            var res = solution.AddStrings(num1, num2);

            // assert
            res.Should().Be("31227");
        }

        [Test]
        public void When_Foo2()
        {
            // arrange 
            var solution = new Solution();
            var num1 = "1";
            var num2 = "9";

            // act
            var res = solution.AddStrings(num1, num2);

            // assert
            res.Should().Be("10");
        }

        [Test]
        public void When_Foo3()
        {
            // arrange 
            var solution = new Solution();
            var num1 = "9";
            var num2 = "99";

            // act
            var res = solution.AddStrings(num1, num2);

            // assert
            res.Should().Be("108");
        }
    }
}