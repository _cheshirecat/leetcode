﻿using FluentAssertions;
using GoogleLeetCode.HappyNumber;
using NUnit.Framework;

namespace GoogleLeetCodeTests.HappyNumber
{
    [TestFixture]
    public class IsHappyTests
    {
        [Test]
        public void When_InputIsHappy()
        {
            // arrange
            var solution = new Solution();
            var input = 97;

            // act
            var res = solution.IsHappy(input);

            // assert
            res.Should().BeTrue();
        }

        [Test]
        public void When_InputIsNotHappy()
        {
            // arrange
            var solution = new Solution();
            var input = 18;

            // act
            var res = solution.IsHappy(input);

            // assert
            res.Should().BeFalse();
        }
    }
}
